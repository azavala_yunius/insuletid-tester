package com.i.insuletidtester;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SingInActivity extends BaseActivity {

    private String code;
    private String codeVerifier;
    String scope = "openid+offline_access+profile+address+phone+email";
    private ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final WebView mWebView = findViewById(R.id.wevView);
        final WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        mWebView.loadUrl(this.getDominio());


        codeVerifier();
        codeChange();
        String code1 = code;
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 64;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String state = buffer.toString();

        String redirect=this.getDominio();
        String callback="";
        int indexUrl=redirect.indexOf("ReturnUrl=");
        if(indexUrl!=-1){
            callback=redirect.substring(indexUrl+10,redirect.length());
        }
        final String dataUrl = SingInActivity.this.getServerUlr() + "/oauth2/default/v1/authorize?client_id=" + SingInActivity.this.getIdOkta() + "&response_type=code&response_mode=fragment&scope=" + scope + "&redirect_uri=" + "horizon://callback"+ "&state=" + state + "&code_challenge_method=S256&code_challenge=" + codeVerifier;
        CookieManager.getInstance().setAcceptCookie(true);
        final String finalCallback = callback;
        mWebView.setWebViewClient(new WebViewClient() {
        int pagina=0;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals(finalCallback)) {
                    mWebView.loadUrl(dataUrl);
                    return false;
                } else if (url.startsWith("horizon://callback#code")&&(pagina==0)) {
                    pagina=1;
                    progressDialog = new ProgressDialog(SingInActivity.this, R.style.MyDialogTheme);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setTitle("Login in...");
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.setMessage("Please wait");
                    progressDialog.show();
                    int index = url.indexOf("code");
                    int indexEnd = url.indexOf("&state");
                    if ((index != -1) && (indexEnd != -1)) {
                        String resultCode = url.substring(index + 5, indexEnd);
                        getTokenWithCode(SingInActivity.this.getIdOkta(), SingInActivity.this.getServerUlr(), resultCode, code, "horizon://callback");
                        mWebView.setVisibility(View.GONE);
                        return false;
                    }
                }

                    return  false;
                }

        });

    }


    private void codeVerifier() {
        SecureRandom sr = new SecureRandom();
        byte[] cod = new byte[32];
        sr.nextBytes(cod);
        String code1 = Base64.encodeToString(cod, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING);
        code = code1.replace('+', '-').replace('/', '_').replace("=", "");

    }

    private void codeChange() {
        byte[] bytes = new byte[0];
        try {
            bytes = code.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(bytes, 0, bytes.length);
        byte[] digest = md.digest();
        String challenge = new String(Base64.encode(digest, Base64.NO_WRAP));
        codeVerifier = challenge.replace('+', '-').replace('/', '_').replace("=", "");

    }


    public void getTokenWithCode(String clientId, String urlDomain, String code, String
            codeVerified, String callBack) {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody body = new FormBody.Builder().add("scope", scope).add("client_id", clientId).add("code_verifier", codeVerified)
                .add("redirect_uri", callBack).add("code", code).add("grant_type", "authorization_code").build();

        Request request = new Request.Builder()
                .url(urlDomain + "/oauth2/default/v1/token").post(body).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();

                SingInActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            final JSONObject json = new JSONObject(myResponse);
                            String access_token=json.getString("access_token");
                            if(!access_token.equals("")) {
                                Intent intent = new Intent(SingInActivity.this, DetailsActivity.class);
                                intent.putExtra("json", myResponse);
                                progressDialog.dismiss();
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

    }


}
