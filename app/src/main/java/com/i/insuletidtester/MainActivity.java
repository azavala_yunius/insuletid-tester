package com.i.insuletidtester;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton custom = findViewById(R.id.settings);
        custom.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
            intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            }


        });

        Button singIn=findViewById(R.id.signIn);
        singIn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, SingInActivity.class);
                startActivity(intent);
            }


        });

    }



}
