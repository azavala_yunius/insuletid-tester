package com.i.insuletidtester;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;


import com.auth0.android.jwt.JWT;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DetailsActivity extends BaseActivity {

    //  private SessionClient sessionClient;
    protected TextView
            id,
            firstName,
            lastName,
            email,
            fullName,
            idJwt,
            idHeader,
            idPayload,
            accessJwt,
            accessHeader,
            accessPayload,
            introspectText;
    private ProgressDialog progressDialog;
    private LinearLayout introspectLayout;
    private boolean auth = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        JSONObject jsonData = new JSONObject();
        Intent intent = getIntent();
        String json = intent.getStringExtra("json");
        try {
            jsonData = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (json.isEmpty()) {
            signOutFromOkta();
        }

        id = findViewById(R.id.textViewLoginId);
        firstName = findViewById(R.id.textViewFirstName);
        lastName = findViewById(R.id.textViewLastName);
        email = findViewById(R.id.textViewEmail);
        fullName = findViewById(R.id.textViewFullName);

        idJwt = findViewById(R.id.textViewJwtIdToken);
        idHeader = findViewById(R.id.textViewHeaderIdToken);
        idPayload = findViewById(R.id.textViewPayloadIdToken);


        accessJwt = findViewById(R.id.textViewJwtAccessToken);
        accessHeader = findViewById(R.id.textViewheaderAccessToken);
        accessPayload = findViewById(R.id.textViewPayloadAccessToken);
        introspectText = findViewById(R.id.textIntrospect);
        introspectLayout=findViewById(R.id.layotIntrospect);
        String idToken = "";
        String accessToken = "";
        try {
            idToken = jsonData.getString("id_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JWT jwt = new JWT(idToken);
        this.setIdText(jwt);
        try {
            accessToken = jsonData.getString("access_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JWT jwt2 = new JWT(accessToken);
        this.setAccessText(jwt2);
        userInfo(DetailsActivity.this.getServerUlr(), accessToken);


        progressDialog = new ProgressDialog(this, R.style.MyDialogTheme);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Login in...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please wait");
        progressDialog.show();

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.inflateMenu(R.menu.logout_menu);

        final String finalAccessToken = accessToken;
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_instrospect:
                        introspectLayout.setVisibility(View.VISIBLE);
                        findViewById(R.id.progressBarIntrospect).setVisibility(View.VISIBLE);
                        introspectText.setText("");
                        introspect(DetailsActivity.this.getServerUlr(), finalAccessToken);
                        return true;

                    case R.id.action_settings:
                        signOutFromOkta();
                        return true;

                }

                return true;
            }
        });


    }


    public void setProfileText(JSONObject jsonObject) {
        try {

            id.setText(jsonObject.getString("preferred_username"));
            firstName.setText(jsonObject.getString("given_name"));
            lastName.setText(jsonObject.getString("family_name"));
            email.setText(jsonObject.getString("email"));
            fullName.setText(jsonObject.getString("name"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();
    }


    public void setIdText(JWT jwt) {
        idJwt.setText(String.format("%s\n", jwt.toString()));
        idHeader.setText(formatStrings(jwt.getHeader()));
        idPayload.setText(formatPayloadId(jwt));
        findViewById(R.id.progressBarId).setVisibility(View.GONE);
    }

    public void setAccessText(JWT jwt) {
        accessJwt.setText(String.format("%s\n", jwt.toString()));
        accessHeader.setText(formatStrings(jwt.getHeader()));
        accessPayload.setText(formatPayloadAccess(jwt));
        findViewById(R.id.progressBarAccess).setVisibility(View.GONE);
    }

    private String formatStrings(Map<String, String> base) {
        StringBuilder tmp = new StringBuilder();
        tmp.append("{\n");
        for (Map.Entry<String, String> entry : base.entrySet()) {
            tmp
                    .append("    \"")
                    .append(entry.getKey())
                    .append("\" : \"")
                    .append(entry.getValue())
                    .append("\"\n");
        }
        tmp.append("}\n");
        return tmp.toString();
    }

    private String formatPayloadId(JWT jwt) {
        StringBuilder tmp = new StringBuilder();
        tmp.append("{\n");
        tmp.append("    \"ver\" : \"").append(jwt.getClaim("ver").asString()).append("\",\n");
        tmp.append("    \"idp\" : \"").append(jwt.getClaim("idp").asString()).append("\",\n");
        tmp.append("    \"preferred_username\" : \"").append(jwt.getClaim("preferred_username").asString()).append("\",\n");
        tmp.append("    \"exp\" : \"").append(jwt.getClaim("exp").asString()).append("\",\n");
        tmp.append("    \"name\" : \"").append(jwt.getClaim("name").asString()).append("\",\n");
        tmp.append("    \"jti\" : \"").append(jwt.getClaim("jti").asString()).append("\",\n");
        tmp.append("    \"sub\" : \"").append(jwt.getClaim("sub").asString()).append("\",\n");
        tmp.append("    \"auth_time\" : \"").append(jwt.getClaim("auth_time").asString()).append("\",\n");
        tmp.append("    \"email\" : \"").append(jwt.getClaim("email").asString()).append("\",\n");
        tmp.append("    \"iss\" : \"").append(jwt.getClaim("iss").asString()).append("\",\n");
        tmp.append("    \"iat\" : \"").append(jwt.getClaim("iat").asString()).append("\",\n");
        tmp.append("    \"at_hash\" : \"").append(jwt.getClaim("at_hash").asString()).append("\",\n");
        tmp.append("    \"aud\" : \"").append(jwt.getClaim("aud").asString()).append("\",\n");
        tmp.append("    \"amr\" : [\n");
        for (String st : jwt.getClaim("amr").asList(String.class)) {
            tmp.append("        \"").append(st).append("\",\n");
        }
        tmp.append("    ]\n");
        tmp.append("}\n");
        return tmp.toString();
    }

    private String formatPayloadAccess(JWT jwt) {
        StringBuilder tmp = new StringBuilder();
        tmp.append("{\n");
        tmp.append("    \"ver\" : \"").append(jwt.getClaim("ver").asString()).append("\",\n");
        tmp.append("    \"sub\" : \"").append(jwt.getClaim("sub").asString()).append("\",\n");
        tmp.append("    \"exp\" : \"").append(jwt.getClaim("exp").asString()).append("\",\n");
        tmp.append("    \"iat\" : \"").append(jwt.getClaim("iat").asString()).append("\",\n");
        tmp.append("    \"scp\" : [\n");
        for (String st : jwt.getClaim("scp").asList(String.class)) {
            tmp.append("        \"").append(st).append("\",\n");
        }
        tmp.append("    ],\n");
        tmp.append("    \"cid\" : \"").append(jwt.getClaim("cid").asString()).append("\",\n");
        tmp.append("    \"uid\" : \"").append(jwt.getClaim("uid").asString()).append("\",\n");
        tmp.append("    \"aud\" : \"").append(jwt.getClaim("aud").asString()).append("\",\n");
        tmp.append("    \"jti\" : \"").append(jwt.getClaim("jti").asString()).append("\",\n");
        tmp.append("    \"iss\" : \"").append(jwt.getClaim("iss").asString()).append("\"\n");
        tmp.append("}\n");
        return tmp.toString();
    }

    private void signOutFromOkta() {
        ProgressDialog progressDialog = new ProgressDialog(DetailsActivity.this, R.style.MyDialogTheme);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Login out...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please wait");
        progressDialog.show();

        if (progressDialog.isShowing()) progressDialog.dismiss();
        Intent intent = new Intent(DetailsActivity.this, MainActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }

    public void userInfo(String url, String acess_token) {

        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        OkHttpClient client = new OkHttpClient();

        String postBody = "";
        RequestBody body = RequestBody.create(MEDIA_TYPE, postBody);

        Request request = new Request.Builder()
                .url(DetailsActivity.this.getServerUlr() + "/oauth2/default/v1/userinfo")
                .header("Authorization", "Bearer " + acess_token)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();

                DetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final JSONObject json = new JSONObject(myResponse);
                            setProfileText(json);
                        } catch (JSONException e) {
                            JSONObject json = new JSONObject();
                            e.printStackTrace();
                        }

                    }
                });
            }
        });


    }


    public void introspect(String url, String acess_token) {

        MediaType MEDIA_TYPE = MediaType.parse("application/x-www-form-urlencoded");
        OkHttpClient client = new OkHttpClient();

        String postBody = "client_id=" + DetailsActivity.this.getIdOkta() + "&token_type_hint=access_token&token=" + acess_token;
        RequestBody body = RequestBody.create(MEDIA_TYPE, postBody);

        RequestBody body1 = new FormBody.Builder()
                .addEncoded("client_id", DetailsActivity.this.getIdOkta())
                .addEncoded("token_type_hint", "access_token")
                .addEncoded("token", acess_token)
                .build();

        Request request = new Request.Builder()
                .url(DetailsActivity.this.getServerUlr() + "/oauth2/default/v1/introspect")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(body1)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String myResponse = response.body().string();

                DetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final JSONObject json = new JSONObject(myResponse);
                            introspectText.setText(introspectString(json));
                        } catch (JSONException e) {
                            findViewById(R.id.progressBarIntrospect).setVisibility(View.GONE);
                            JSONObject json = new JSONObject();
                            e.printStackTrace();
                        }

                    }
                });
            }
        });


    }

    public String introspectString(JSONObject json) {
        StringBuilder tmp = new StringBuilder();
        String active="";
        try {
            active=json.getString("active");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(active.equals("true")) {
            tmp.append("{\n");
            try {
                tmp.append("    \"exp\" :").append(json.getString("exp")).append(",\n");
                tmp.append("    \"iat\" :").append(json.getString("iat")).append(",\n");
                tmp.append("    \"token_type\" : \"").append(json.getString("token_type")).append("\",\n");
                tmp.append("    \"customerID\" : \"").append(json.getString("customerID")).append("\",\n");
                tmp.append("    \"uid\" : \"").append(json.getString("uid")).append("\",\n");
                tmp.append("    \"username\" : \"").append(json.getString("username")).append("\",\n");
                tmp.append("    \"iss\" : \"").append(json.getString("iss")).append("\",\n");
                tmp.append("    \"scope\" : \"").append(json.getString("scope")).append("\",\n");
                tmp.append("    \"client_id\" : \"").append(json.getString("client_id")).append("\",\n");
                tmp.append("    \"sub\" : \"").append(json.getString("sub")).append("\",\n");
                tmp.append("    \"jti\" : \"").append(json.getString("jti")).append("\",\n");
                tmp.append("    \"aud\" : \"").append(json.getString("aud")).append("\",\n");
                tmp.append("    \"active\" :").append(json.getString("active")).append("\n");


            } catch (JSONException e) {
                e.printStackTrace();
            }
            tmp.append("}\n");
        }else{
            tmp.append("{\n");
            try {
                tmp.append("    \"active\" :").append(json.getString("active")).append("\n");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            tmp.append("}\n");
        }
        findViewById(R.id.progressBarIntrospect).setVisibility(View.GONE);
        return tmp.toString();
    }


}
