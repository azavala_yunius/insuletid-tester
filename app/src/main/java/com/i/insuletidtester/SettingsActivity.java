package com.i.insuletidtester;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class SettingsActivity extends BaseActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_preferences));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final TextInputEditText dominio = findViewById(R.id.textDomain);
        final TextInputEditText client = findViewById(R.id.textClient);
        final TextInputEditText server = findViewById(R.id.textServer);
        final TextInputEditText scheme = findViewById(R.id.textScheme);

        dominio.setText(this.getDominio());
        client.setText(this.getIdOkta());
        server.setText(this.getServerUlr());
        scheme.setText(this.getScheme());

        Button save = findViewById(R.id.buttonSaveSettings);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SettingsActivity.this.setDominio(Objects.requireNonNull(dominio.getText()).toString());
                SettingsActivity.this.setIdOkta(Objects.requireNonNull(client.getText()).toString());
                 SettingsActivity.this.setServerUrl(Objects.requireNonNull(server.getText()).toString());
                SettingsActivity.this.setScheme(Objects.requireNonNull(scheme.getText()).toString());
                SettingsActivity.this.finish();
            }
        });

    }



}
