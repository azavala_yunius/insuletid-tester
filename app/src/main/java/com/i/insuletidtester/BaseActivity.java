package com.i.insuletidtester;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity  extends AppCompatActivity {

    private SharedPreferences oktaData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oktaData = getSharedPreferences("oktaData", Activity.MODE_PRIVATE);
    }

    public String getDominio(){
        return this.oktaData.getString("dominio",getString(R.string.url));
    }

    public String getIdOkta(){
        return this.oktaData.getString("id",getString(R.string.clientId));
    }

    public String getServerUlr(){
        return this.oktaData.getString("surl",getString(R.string.serverUlr));
    }

    public String getScheme(){
        return this.oktaData.getString("urlscheme",getString(R.string.scheme));
    }

    public void setDominio(String dominio){
        this.oktaData.edit().putString("dominio", dominio).apply();
    }

    public void setIdOkta(String id){
        this.oktaData.edit().putString("id", id).apply();
    }

    public void setServerUrl(String idp){
        this.oktaData.edit().putString("surl", idp).apply();
    }

    public void setScheme(String api){
        this.oktaData.edit().putString("urlscheme", api).apply();
    }

    public void limpiar(){
        this.oktaData.edit().clear().apply();
    }

    public void saveUser(String token){
        this.oktaData.edit().putString("user",token).apply();
    }

    public String getUser(){
        return this.oktaData.getString("user", null);
    }

    public void removeUser(){
        this.oktaData.edit().remove("user").apply();
    }

}
